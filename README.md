# emuLimits

## Setup the software

After checking out the project, you must setup a valid ROOT version. I use `6.22/08` locally. Once you have setup a ROOT version (e.g. you have a $ROOTSYS env var pointing at ROOT), you can then do:

```asm
mkdir build; cd build
cmake ../xroofit
make -j
source setup.sh
```

You need to source the setup script each time you start a new session. It just puts xroofit in the library path for ROOT to be able to load it.

Confirm it all works by opening e.g. python and doing:

```asm
import ROOT
b = ROOT.xRooBrowser()
```

## Running the limits

You must create a directory (or symbolic link) called `inputs` which will contain all the histogram files used for the limit setting. 

Then from the top directory (not the build directory) you can run:

```asm
python -i runLimits.py
```

This will produce workspace files in the top directory, one for the bkg-only model and one for each signal grid point. When you run the script a second time it will reuse any existing workspace files it created so if you want to make a change that will rebuild the workspace just delete (or move) the existing workspace file and a new one will be created.

You can explore any of the workspaces by opening them and booting up an xRooBrowser:

```asm
root emuBkgModel_RPV.root
xRooBrowser b;
```
