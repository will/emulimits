RPC_width = {
    (250, 0)    : 0.314717575,
    (250, 50)   : 0.288670743,
    (250, 100)  : 0.2647,
    (250, 150)  : 0.2397,
    (250, 175)  : 0.2272,
    (250, 200)  : 0.2147,
    (500, 0)    : 0.642170964,
    (500, 50)   : 0.629133503,
    (500, 100)  : 0.590416351,
    (500, 150)  : 0.528509059,
    (500, 175)  : 0.490090869,
    (500, 200)  : 0.4799,
    (800, 0)    : 1.0392693,
    (800, 50)   : 1.03112033,
    (800, 100)  : 1.00672096,
    (800, 150)  : 0.966664415,
    (800, 175)  : 0.941057224,
    (800, 200)  : 0.9309,
    (1200, 0)   : 1.56374128,
    (1200, 50)  : 1.55823264,
    (1200, 100) : 1.54182505,
    (1200, 150) : 1.51469654,
    (1200, 175) : 1.49719868,
    (1200, 200) : 1.4918,
    (1500, 0)   : 1.95543182,
    (1500, 50)  : 1.93788915,
    (1500, 100) : 1.93773614,
    (1500, 150) : 1.91586461,
    (1500, 175) : 1.90172935,
    (1500, 200) : 1.8967,
    (1750, 0)   : 2.26807583,
    (1750, 50)  : 2.26416014,
    (1750, 100) : 2.25269007,
    (1750, 150) : 2.23372713,
    (1750, 175) : 2.22146477,
    (1750, 200) : 2.2136
}


RPVpRPC_width = {
    (250, 0)    : 4.45410777,
    (250, 50)   : 4.43019251,
    (250, 100)  : 4.4041,
    (250, 150)  : 4.3791,
    (250, 175)  : 4.3666,
    (250, 200)  : 4.3541,
    (500, 0)    : 23.900488,
    (500, 50)   : 23.8880277,
    (500, 100)  : 23.8510096,
    (500, 150)  : 23.79188,
    (500, 175)  : 23.7552228,
    (500, 200)  : 23.757,
    (800, 0)    : 44.4642368,
    (800, 50)   : 44.4562288,
    (800, 100)  : 44.432233,
    (800, 150)  : 44.3928649,
    (800, 175)  : 44.3677174,
    (800, 200)  : 44.356,
    (1200, 0)   : 70.4194902,
    (1200, 50)  : 70.4140158,
    (1200, 100) : 70.3976881,
    (1200, 150) : 70.3706945,
    (1200, 175) : 70.3532887,
    (1200, 200) : 70.347,
    (1500, 0)   : 89.2797807,
    (1500, 50)  : 89.2753075,
    (1500, 100) : 89.2620704,
    (1500, 150) : 89.2401683,
    (1500, 175) : 89.2260152,
    (1500, 200) : 89.226,
    (1750, 0)   : 104.961407,
    (1750, 50)  : 104.957503,
    (1750, 100) : 104.94605,
    (1750, 150) : 104.927111,
    (1750, 175) : 104.914864,
    (1750, 200) : 104.91
}


def BR_rescaling(smuon, n1, coupling):
    if coupling==1: return 1
    if smuon < 175: return 1
    if (smuon,n1) not in RPC_width:
        print("WARNING, masses not in rescaling dict:",smuon,n1)
        return 1
    rpc = RPC_width[(smuon, n1)]
    rpv = RPVpRPC_width[(smuon, n1)] - rpc
    BRref = rpc/(rpc+rpv)
    BRnew = rpc/(rpc+coupling*coupling*rpv)
    print(rpc,rpv,BRref,BRnew)
    return BRnew/BRref

