import ROOT
import rpv
import math
from BR_rescaling import *
import glob
try:
    from tqdm import tqdm # for progress bars
except ImportError:
    # if tqdm not available use a no-op class
    def tqdm(iterator, *args, **kwargs):
        return iterator
import sys
import os

canv = ROOT.TCanvas("myCanvas","emu Model",500,400)

planeName = "LQ" # switch RPV or LQ plane
isUnblinded = True
datelabel = "210809"

def addHistogram(model, h,sName, BRscaling=1.0):

    s = str(h.GetName())
    planeName,regionName,channelName,sampleName,variationName = s.split('_',4)

    # get the channel, create if not existing
    chanName = regionName+"_"+channelName
    if not model.contains(chanName): model.Vary(chanName) # adding a channel to a model is a 'variation' of the model
    c = model[chanName]
    c.SetTitle(regionName+" "+ ("e^{+}#mu^{-}" if "ePmuM" in channelName else "e^{-}#mu^{+}") )

    if sName != "": sampleName = sName # allow overriding sample name

    varUp = "1up"
    if "__" in variationName: variationName, varUp = variationName.rsplit("__",1)


    if sampleName == "data":

        if variationName == "Nominal":
            # need to create a 'sm' sample and then set the data
            uniform = h.Clone("sm")
            uniform.SetFillColor(ROOT.kGreen)
            uniform.SetTitle("SM")
            uniform.Reset() # clear
            for i in range(0,uniform.GetNbinsX()): uniform.SetBinContent(i+1, 1)
            samp = c["samples"].Add(uniform) # although uniform it actually represents the bin density
            # scale each bin by the per-bin factor (aka shapeFactor)
            # for this region
            if not w.function(regionName+"_smSF"):
                 uniform.SetOption("shape")
                 uniform.SetName(regionName+"_smSF")
                 w.Add(uniform)
            shapeFactor = samp.Multiply(regionName+"_smSF")
            #shapeFactor = c["samples"].Add(regionName+"_smSF")
            #
            if channelName == "OSeMmuP":
                 # use the denominator histogram to set the initial value and range of the shape factors
                 for ii,p in enumerate(shapeFactor.pars()):
                     p.setVal(h.GetBinContent(ii+1))
                     p.setRange(0, p.getVal()*5)
                     p.setConstant(False)

            #if model == m:
            c.datasets()["obsData"].Add(h)

            # # create a nominal dataSF factor for the channel too
            uniform.SetName("dataSF")
            uniform.SetOption("alias=dataSF")
            c["samples"].Multiply(uniform) # multiply the 'samples' collection will mean sf applied to all samples
            # # set all bins to 1
            # for i in range(dataSF.GetXaxis().GetNbins()): dataSF.SetBinContent(i+1,1)
        else:
            # data systematic variations are added as factors of value 1 +/- relative uncert
            hcopy = h.Clone("{}={}".format(variationName,1 if varUp=="1up" else -1))
            for i in range(h.GetNbinsX()):
                nomData = c.datasets()["obsData"].GetBinContent(i+1)
                hcopy.SetBinContent(i+1, (hcopy.GetBinContent(i+1)/nomData) if nomData!=0 else 1)
                hcopy.SetBinError(i+1,0)
            # #c.samples()[0].factors().Print()
            c["samples"][0]["dataSF"].Vary(hcopy)#.SetContents(hcopy,variationName,varUp=="1up")
            if c["samples"].pars()[variationName].constraints().empty():
                c["samples"].pars()[variationName].constraints().Add("gaussian")
            # #c.samples()[0].factors()["dataSF"].variations().add(hcopy, variationName + ("_up" if varUp=="1up" else "_down"))

    elif "theory" not in variationName: # skip "dummy theory" uncertainties
        # regular sample
        if "310" in sampleName and planeName=="RPV": h.Scale(BRscaling)
        h.SetName(sampleName)
        h.SetTitle(sampleName)
        h.SetFillColor(ROOT.kRed if sampleName=="fakes" else ROOT.kBlue)

        if variationName=="Nominal":
            h.SetOption("statPrefix=stat_{}_{}".format(chanName,sampleName))
            c["samples"][sampleName].SetContents(h)
            if sampleName != "fakes":
                # signal sample, check is scaled by mu
                if not c["samples"][sampleName].factors().contains("mu"): c["samples"][sampleName].Multiply("mu")

        else:
            varyName = "{}={}".format(variationName,1 if varUp=="1up" else -1)
            #vary = c["samples"][sampleName].variations().find(varyName)
            #if vary.get(): c["samples"][sampleName].Vary(varyName)
            # should clear h uncertainty
            for i in range(0,h.GetNbinsX()): h.SetBinError(i+1,0)
            #c["samples"][sampleName][sampleName].variations()[varyName].SetContents(h) # equivalent of next line but more explicit
            c["samples"][sampleName][varyName].SetContents(h)
            if c["samples"].pars()[variationName].constraints().empty():
                c["samples"].pars()[variationName].constraints().Add("gaussian")



def loadFile(model, filename, sampleName="", BRscaling=1.0):
    f1 = ROOT.TFile(filename);

    # get list of histograms
    histNames = [k.GetName() for k in f1.GetListOfKeys()]
    # ensure Nominal histograms are loaded before the respective variation histograms
    doneHists = []
    for k in f1.GetListOfKeys():
        s = str(k.GetName())
        if s in doneHists: continue
        if not s.startswith(planeName): continue
        if s.endswith(('1up','1down','ResoPara','ResoPerp','theory10','theory20')):
            # ensure the nominal case has been handled first
            nomName = '_'.join( s.split('_')[:4] ) + "_Nominal"
            if nomName not in doneHists:
                addHistogram(model, f1.Get(nomName), sampleName, BRscaling=BRscaling)
                doneHists.append(nomName)
        addHistogram(model, f1.Get(s), sampleName, BRscaling=BRscaling)
        doneHists.append(s)

if not os.path.exists("emuBkgModel_%s.root"%planeName):
    w = ROOT.xRooNode("RooWorkspace","combined","emu workspace")
    m = w.Add("simPdf","model")
    fakeFile = list(glob.glob("inputs/*_Fakes*.root"))[0]
    dataFile = list(glob.glob("inputs/*_data*.root"))[0]
    loadFile(m,fakeFile,"fakes")
    loadFile(m,dataFile)
    w.datasets()["obsData"].SetTitle("Data")


    for c in m.variations(): # channels are "variations" of a model
        # scale all numerators by R
        if "OSePmuM" in c.GetName():
            # is numerator, so scale the 'SM' component by R
            c["sm"].Multiply("R","norm")
            # scale by 6% uncertainty too, to cover variations in R
            print("Scaling {} by R uncertainty".format(c.GetName()))
            factor = c["sm"].Multiply(c.GetName()[11:]+"_uncertR","shape") # 11: is to strip "channelCat=" off channel name
            for v in factor.pars():
                # float each parameter and give it a gaussian constraint
                v.setVal(1);v.setRange(0,2);v.setConstant(False)
                v.constraints().Add("gaussian(1,%s)" % ("0.06" if planeName=="RPV" else "0.09"))
        # scale fake estimates by 18.5% uncertainty
        c["samples"]["fakes"].Multiply(c.GetName()[11:]+"_fakeOverall","norm").Constrain("gaussian(1,0.21)")


    # define the various combinations of channels that are worked with
    if planeName == "RPV":
        m.obs()["channelCat"].setRange("CR","x1y1_OSeMmuP,x1y1_OSePmuM")
        m.obs()["channelCat"].setRange("VR_denom","x1y2_OSeMmuP,x2y1_OSeMmuP")
        m.obs()["channelCat"].setRange("VR_numer","x1y2_OSePmuM,x2y1_OSePmuM")
        m.obs()["channelCat"].setRange("SR_denom","x2y2_OSeMmuP")
        m.obs()["channelCat"].setRange("SR_numer","x2y2_OSePmuM")
    else:
        m.obs()["channelCat"].setRange("CR","x1y2_OSeMmuP,x1y2_OSePmuM")
        m.obs()["channelCat"].setRange("VR_denom","x1y1_OSeMmuP,x2y2_OSeMmuP")
        m.obs()["channelCat"].setRange("VR_numer","x1y1_OSePmuM,x2y2_OSePmuM")
        m.obs()["channelCat"].setRange("SR_denom","x2y1_OSeMmuP")
        m.obs()["channelCat"].setRange("SR_numer","x2y1_OSePmuM")

    ## make the expected data in the SR_numer by running a fit to CR+SR_denom
    m.SetRange("CR,SR_denom")
    obsData = w.datasets()["obsData"]
    fr = m.createNLL("obsData").minimize() # = m.fitTo(obsData)
    fr.Print()
    m.SetRange("SR_numer")
    exp_SR = m.createNLL("obsData").generate(True) # = m.generate(True)
    obs = m.obs().argList()
    # make a dataset with all the regions except SR_numer
    partData = obsData.reduce(ROOT.RooFit.SelectVars(obsData.get().get()),ROOT.RooFit.CutRange("CR,VR_denom,VR_numer,SR_denom"))
    partData.SetName("partData");partData.SetTitle("Partial Data")
    # add in the expected data
    for r in range(0,exp_SR.first.numEntries()):
        obs.assignValueOnly(exp_SR.first.get(r))
        partData.add( obs, exp_SR.first.weight() )
    w.Import(partData) # add to workspace
    w.SaveAs("emuBkgModel_%s.root"%planeName)

    # fit the dataset to CR+denoms before extracting the post-fit yields
    # not including SR_numer in the fit so that SR appears consistent with how the exclusion contours
    # appear wrt the expected contour (if included in post-fit the difference between obs and asimov will be lost
    # inside the global observable aux measurements).
    m.SetRange("CR,SR_denom,VR_denom")
    m.createNLL("obsData").minimize()

    # hide all the denominators from being plotted
    for c in m.variations():
        if "eMmuP" in c.GetName(): c.SetHidden(True)
    m.Draw("esignificance") # draws with error bars and significance
    m.datasets()["obsData"].Draw("same")
    canv.SaveAs("debugplot_"+planeName+".eps")
    canv.SaveAs("debugplot_"+planeName+".pdf")
    canv.SaveAs("debugplot_"+planeName+".png")

    srforyields = "x2y2" if planeName=="RPV" else "x2y1"
    crforyields = "x1y1" if planeName=="RPV" else "x1y2"
    vr1foryields = "x1y2" if planeName=="RPV" else "x1y1"
    vr2foryields = "x2y1" if planeName=="RPV" else "x2y2"

    with open("yields_{0}_{1}.json".format(planeName,datelabel),'w') as ff:
      tmp2 = sys.stdout
      sys.stdout=ff
      yields = ""
      histos = ''

      yields = yields+'{\n'
      srvar = "EtMissSignificance" if planeName=="RPV" else "Hp"
      histo = 'OSePmuM_SR_'+planeName+'_'+srvar
      yields = yields + '"'+histo+'":{\n'
      histos = histos + '"' + histo + '",'
      r = m[srforyields+"_OSePmuM"]
      SRlowedge = r["sm"].GetXaxis().GetBinLowEdge(1)
      yields = yields+'"SM":{\n'
      yields = yields+'"'+str(SRlowedge)+'":['+str(r["sm"].GetBinContent(1))+','+str(r["sm"].GetBinError(1))+']\n'
      yields = yields+'},\n'
      yields = yields+'"Significance":{\n'
      yields = yields+'"'+str(SRlowedge)+'":['+str(ROOT.gPad.GetPrimitive(r.GetName()).GetPrimitive("auxPad").GetPrimitive("obsData").GetBinContent(1))+',0.0]\n'
      yields = yields+'},\n'
      yields = yields+'"Fakes":{\n'
      yields = yields+'"'+str(SRlowedge)+'":['+str(r["fakes"].GetBinContent(1))+','+str(r["fakes"].GetBinError(1))+']\n'
      yields = yields+'},\n'
      yields = yields+'"data":{\n'
      yields = yields+'"'+str(SRlowedge)+'":['+str(r.datasets()["obsData"].GetBinContent(1))+','+str(math.sqrt(r.datasets()["obsData"].GetBinContent(1)))+']\n'
      yields = yields+'}\n'
      yields = yields+'},\n'

      histo = 'OSePmuM_CR_'+planeName+'_'+srvar
      yields = yields+'"'+histo+'":{\n'
      histos = histos + '"' + histo + '",'

      CRlowedge = m[crforyields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(1)
      yields = yields+'"SM":{\n'
      r = m[crforyields+"_OSePmuM"]
      yields = yields+'"'+str(CRlowedge)+'":['+str(r["sm"].GetBinContent(1))+','+str(r["sm"].GetBinError(1))+']\n'
      yields = yields+'},\n'
      yields = yields+'"Significance":{\n'
      yields = yields+'"'+str(CRlowedge)+'":['+str(ROOT.gPad.GetPrimitive(r.GetName()).GetPrimitive("auxPad").GetPrimitive("obsData").GetBinContent(1))+',0.0]\n'
      yields = yields+'},\n'
      yields = yields+'"Fakes":{\n'
      yields = yields+'"'+str(CRlowedge)+'":['+str(r["fakes"].GetBinContent(1))+','+str(r["fakes"].GetBinError(1))+']\n'
      yields = yields+'},\n'
      yields = yields+'"data":{\n'
      yields = yields+'"'+str(CRlowedge)+'":['+str(r.datasets()["obsData"].GetBinContent(1))+','+str(math.sqrt(r.datasets()["obsData"].GetBinContent(1)))+']\n'
      yields = yields+'}\n'
      yields = yields+'},\n'

      vr1var = "EtMissSignificance" if planeName=="RPV" else "Hp"
      histo = 'OSePmuM_VR1_'+planeName+'_'+vr1var
      yields = yields+'"'+histo+'":{\n'
      histos = histos + '"' + histo + '",'

      nbinsvr1 = m[vr1foryields+"_OSePmuM"]["sm"].GetXaxis().GetNbins()
      yields = yields+'"SM":{\n'
      for b in range(1,nbinsvr1+1):
        comma = "," if b<nbinsvr1 else ""
        VR1lowedge = m[vr1foryields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(b)
        yields = yields+'"'+str(VR1lowedge)+'":['+str(m[vr1foryields+"_OSePmuM"]["sm"].GetBinContent(b))+','+str(m[vr1foryields+"_OSePmuM"]["sm"].GetBinError(b))+']'+comma+'\n'
      yields = yields+'},\n'
      yields = yields+'"Significance":{\n'
      for e in range(1,nbinsvr1+1):
       comma = "," if e<nbinsvr1 else ""
       VR1lowedge = m[vr1foryields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(e)
       yields = yields+'"'+str(VR1lowedge)+'":['+str(ROOT.gPad.GetPrimitive("channelCat=%s_OSePmuM" % vr1foryields).GetPrimitive("auxPad").GetPrimitive("obsData").GetBinContent(e))+',0.0]'+comma+'\n'
      yields = yields+'},\n'
      yields = yields+'"Fakes":{\n'
      for c in range(1,nbinsvr1+1):
        comma = "," if c<nbinsvr1 else ""
        VR1lowedge = m[vr1foryields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(c)
        yields = yields+'"'+str(VR1lowedge)+'":['+str(m[vr1foryields+"_OSePmuM"]["fakes"].GetBinContent(c))+','+str(m[vr1foryields+"_OSePmuM"]["fakes"].GetBinError(c))+']'+comma+'\n'
      yields = yields+'},\n'
      yields = yields+'"data":{\n'
      for d in range(1,nbinsvr1+1):
        comma = "," if d<nbinsvr1 else ""
        VR1lowedge = m[vr1foryields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(d)
        yields = yields+'"'+str(VR1lowedge)+'":['+str(m[vr1foryields+"_OSePmuM"].datasets()["obsData"].GetBinContent(d))+','+str(math.sqrt(m[vr1foryields+"_OSePmuM"].datasets()["obsData"].GetBinContent(d)))+']'+comma+'\n'
      yields = yields+'}\n'
      yields = yields+'},\n'

      vr2var = "MT2Leptons" if planeName=="RPV" else "EtMissSignificance"
      histo = 'OSePmuM_VR2_'+planeName+'_'+vr2var
      yields = yields+'"'+histo+'":{\n'
      histos = histos + '"' + histo + '"'

      nbinsvr2 = m[vr2foryields+"_OSePmuM"]["sm"].GetXaxis().GetNbins()
      yields = yields+'"SM":{\n'
      for bb in range(1,nbinsvr2+1):
        comma = "," if bb<nbinsvr2 else ""
        VR1lowedge = m[vr2foryields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(bb)
        yields = yields+'"'+str(VR1lowedge)+'":['+str(m[vr2foryields+"_OSePmuM"]["sm"].GetBinContent(bb))+','+str(m[vr2foryields+"_OSePmuM"]["sm"].GetBinError(bb))+']'+comma+'\n'
      yields = yields+'},\n'
      yields = yields+'"Significance":{\n'
      for ee in range(1,nbinsvr2+1):
       comma = "," if ee<nbinsvr2 else ""
       VR1lowedge = m[vr2foryields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(ee)
       yields = yields+'"'+str(VR1lowedge)+'":['+str(ROOT.gPad.GetPrimitive("channelCat=%s_OSePmuM" % vr2foryields).GetPrimitive("auxPad").GetPrimitive("obsData").GetBinContent(ee))+',0.0]'+comma+'\n'
      yields = yields+'},\n'
      yields = yields+'"Fakes":{\n'
      for cc in range(1,nbinsvr2+1):
        comma = "," if cc<nbinsvr2 else ""
        VR1lowedge = m[vr2foryields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(cc)
        yields = yields+'"'+str(VR1lowedge)+'":['+str(m[vr2foryields+"_OSePmuM"]["fakes"].GetBinContent(cc))+','+str(m[vr2foryields+"_OSePmuM"]["fakes"].GetBinError(cc))+']'+comma+'\n'
      yields = yields+'},\n'
      yields = yields+'"data":{\n'
      for dd in range(1,nbinsvr2+1):
        comma = "," if dd<nbinsvr2 else ""
        VR1lowedge = m[vr2foryields+"_OSePmuM"]["sm"].GetXaxis().GetBinLowEdge(dd)
        yields = yields+'"'+str(VR1lowedge)+'":['+str(m[vr2foryields+"_OSePmuM"].datasets()["obsData"].GetBinContent(dd))+','+str(math.sqrt(m[vr2foryields+"_OSePmuM"].datasets()["obsData"].GetBinContent(dd)))+']'+comma+'\n'
      yields = yields+'}\n'
      yields = yields+'},\n'
      yields = yields+'"metadata":{\n'
      yields = yields+'"backgrounds":["data","Fakes","SM","Significance"],\n'
      yields = yields+'"grids":["RPV","LQ"],\n'
      yields = yields+'"histograms":['+histos+']\n'
      yields = yields+'}\n'
      yields = yields+'}\n'
      print(yields)
      sys.stdout = tmp2


files = list(glob.glob("inputs/*_310*.root")) if planeName=="RPV" else \
    (list(glob.glob("inputs/*_311*.root")) + list(glob.glob("inputs/*_99*.root")))

sig_w = {}

results = []

fitConfig = ROOT.xRooFit.defaultFitConfig()
fitConfig.SetParabErrors(False) # turn off hesse, isn't needed if we dont care about evaluating uncerts.

for i,f in enumerate(tqdm(files)):
    dsid = f.split("_")[1][:6]
    sigFile = "emuSig{}.root".format(dsid)
    coupling = 1.0
    totalBRscale = 1.0
    if planeName=="RPV":
      if not int(dsid) in rpv.points.keys():
        print( dsid,"argh point missing!")
        break
      rpvpoint = rpv.points[int(dsid)]
      smuonmass, n1mass = rpvpoint['ymass'], rpvpoint['xmass']
      BRscale = BR_rescaling(smuonmass, n1mass, coupling)
      totalBRscale = BRscale*coupling*coupling # BR changes but also total cross section increases!
    if not os.path.exists(sigFile):
        ww = ROOT.xRooNode("RooWorkspace","combined","Signal {}".format(dsid))
        ww.Add(ROOT.RooRealVar("mu","mu",1,-0.5,10))
        sig_model = ww.Add("sig{}".format(dsid),"model")
        print("totalBRscale  = ",totalBRscale)
        loadFile(sig_model,f,"signal" if sig_model.GetName()=="simPdf" else "", BRscaling=totalBRscale)
        ww.SaveAs("emuSig{}.root".format(dsid))

    ww = ROOT.xRooNode("emuBkgModel_%s.root"%planeName)
    ww2 = ROOT.xRooNode(sigFile)
    model = ww["simPdf"] # currently just the bkg-only
    model.Combine( ww2["sig{}".format(dsid)] ) # now is sig+bkg model, and globs of datasets should be updated with sig-specific globs too

    model.SetRange("CR,SR_denom,SR_numer") # only do fits in CR + SR

    nll = model.createNLL("obsData" if isUnblinded else "partData")
    nll.SetFitConfig(fitConfig) # use customized fitConfig

    # order to eval pll vs sigma_mu will marginally affect result a little
    # for now eval pll until sure 'setData' feature of NLLVar working correctly (I think it is)

    pll = nll.pll("mu",1,ROOT.xRooFit.Asymptotics.OneSidedPositive)
    sigma_mu = nll.sigma_mu("mu",1,0)

    print(dsid,pll,sigma_mu)
    pll = pll.first; sigma_mu = sigma_mu.first # ignore uncertainties for now

    p_sb = ROOT.xRooFit.Asymptotics.PValue(ROOT.xRooFit.Asymptotics.OneSidedPositive, pll, 1, 1, sigma_mu, 0)
    p_b = ROOT.xRooFit.Asymptotics.PValue(ROOT.xRooFit.Asymptotics.OneSidedPositive, pll, 1, 0, sigma_mu, 0)

    # get expected pll
    exp_cls = {}
    for ii in range(-1,2):
        exp_pll = ROOT.xRooFit.Asymptotics.k(ROOT.xRooFit.Asymptotics.OneSidedPositive,ROOT.Math.normal_cdf(ii),1,0,sigma_mu,0,100)
        sb = ROOT.xRooFit.Asymptotics.PValue(ROOT.xRooFit.Asymptotics.OneSidedPositive,exp_pll,1,1,sigma_mu,0,100)
        b = ROOT.xRooFit.Asymptotics.PValue(ROOT.xRooFit.Asymptotics.OneSidedPositive,exp_pll,1,0,sigma_mu,0,100)
        exp_cls[ii] = sb/b

    mydict = {"CLs":p_sb/p_b if p_b>0 else (1 if p_sb==0 else 0),
              "CLb":p_b,
              "CLsexp":exp_cls[0],
              "CLsexp_errd":exp_cls[-1],
              "CLsexp_erru":exp_cls[1],
              "DSID":dsid,
              "obs_pll":pll,
              "sigma_mu":sigma_mu}


    results.append(mydict)

with open("results_{0}_lamp231_{1}_{2}.txt".format(planeName,str(coupling).replace('.','p'),datelabel),'w') as f:
    tmp = sys.stdout
    sys.stdout=f
    print(results)
    sys.stdout = tmp
#print(results)
