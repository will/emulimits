export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
setupATLAS
#lsetup 'root 6.06.06-x86_64-slc6-gcc49-opt'
lsetup CMake
#lsetup "root 6.18.04"
lsetup "root 6.22.06"
cd build
source setup.sh
cd ../
